#!/usr/bin/env bash
NVM_VERSION=16.13.2
CHASSIS_PROJECT_ID=123904

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install ${NVM_VERSION}
nvm use ${NVM_VERSION}

# Get gitlab token
GITLAB_TOKEN=""

[ -f .gitlab-token ] && {
  GITLAB_TOKEN=$(<.gitlab-token)
}
[ -f "$DEMI_DIR"/.gitlab-token ] && {
  GITLAB_TOKEN=$(<"$DEMI_DIR"/.gitlab-token)
}
[ -f "$HOME"/.gitlab-token ] && {
  GITLAB_TOKEN=$(<"$HOME"/.gitlab-token)
}

if [[ "" == $GITLAB_TOKEN ]]; then
  echo "Error: Missing personal access token file '${HOME}/.gitlab-token'"
  echo "Get your token from https://gitlab.cern.ch/-/profile/personal_access_tokens"
  return
fi

npm config set @itk-demo-sw:registry https://gitlab.cern.ch/api/v4/projects/${CHASSIS_PROJECT_ID}/packages/npm/
npm config set -- //gitlab.cern.ch/api/v4/projects/${CHASSIS_PROJECT_ID}/packages/npm/:_authToken ${GITLAB_TOKEN}

cd ui

npm install
npm run build 

cd ..
