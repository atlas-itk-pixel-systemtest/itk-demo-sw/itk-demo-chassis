import * as React from 'react';
import {
  PageSection,
  Title,
  Gallery,
  Card,
  CardBody,
  CardTitle,
  CardFooter,
  TextContent,
  Text,
  Skeleton,
} from '@patternfly/react-core';

const modifiers = ['isHoverable', 'isCompact'];

const card1 = "DAQ Node";
const card2 = "Config DB";

const ServiceCard: React.FunctionComponent = (props) => (
  <Card {...modifiers}>
    <CardTitle>{props.type}</CardTitle>
    <CardBody>
      <Skeleton height="100%" width="100%" />
    </CardBody>
    <CardFooter>{props.type}</CardFooter>
  </Card>
);

const Dashboard: React.FunctionComponent = () => (
  <PageSection>
    <Title headingLevel="h1" size="lg">
      ITk Systemtest Microservices
    </Title>
    <TextContent>
      <Text component="h1">Dashboard</Text>
      <Text component="p">This page can contain cards with summary information of online services.</Text>
    </TextContent>
    <Gallery hasGutter>
      <ServiceCard type={card1} />
      <ServiceCard type={card2} />
    </Gallery>
  </PageSection>
);

export { Dashboard };
