import * as React from 'react';
import {
  PageSection,
  // Title,
  // TextContent,
  // Text,
  // Banner,
  // Divider,
  // Drawer,
  // DrawerContent,
  // DrawerContentBody,
} from '@patternfly/react-core';

export const PageTemplate: React.FunctionComponent = (props) => (
  <PageSection>
    {/* <Banner variant="info">Original URL: {props.src}</Banner> */}
    <iframe src={props.src} className="full-iframe" />
  </PageSection>
);
