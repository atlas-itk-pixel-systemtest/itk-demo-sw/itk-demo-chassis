import * as React from 'react';
import {
  Route,
  RouteComponentProps,
  Switch,
  // Redirect,
} from 'react-router-dom';
import { PageSection, Spinner } from '@patternfly/react-core';
import { accessibleRouteChangeHandler } from './utils/utils';
import { Dashboard } from './Dashboard/Dashboard';
import { PageTemplate } from './PageTemplate/PageTemplate';
import { NotFound } from './NotFound/NotFound';
import { useDocumentTitle } from './utils/useDocumentTitle';
import { LastLocationProvider, useLastLocation } from 'react-router-last-location';

let routeFocusTimer: number;
export interface IAppRoute {
  label?: string; // Excluding the label will exclude the route from the nav sidebar in AppLayout
  /* eslint-disable @typescript-eslint/no-explicit-any */
  component: React.ComponentType<RouteComponentProps<unknown>> | React.ComponentType<unknown>;
  /* eslint-enable @typescript-eslint/no-explicit-any */
  exact?: boolean;
  path: string;
  title: string;
  isAsync?: boolean;
  routes?: undefined;
  src?: string;
}

export interface IAppRouteGroup {
  label: string;
  routes: IAppRoute[];
}

export type AppRouteConfig = IAppRoute | IAppRouteGroup;

// const defaultMenu: AppRouteConfig[] = [
//   {
//     component: Dashboard,
//     exact: true,
//     label: "Dashboard",
//     path: "/dashboard",
//     title: "ITk Demo Service | Dashboard",
//   },
//   {
//     component: PageTemplate,
//     exact: true,
//     label: "Template0",
//     path: "/template0",
//     title: "ITk Demo Service | Template0",
//   },
// ];

const componentDict = {
  Dashboard: Dashboard,
  PageTemplate: PageTemplate,
};

// export var routes: AppRouteConfig[] = defaultMenu;
export let routes: AppRouteConfig[] = [];

// a custom hook for sending focus to the primary content container
// after a view has loaded so that subsequent press of tab key
// sends focus directly to relevant content
const useA11yRouteChange = (isAsync: boolean) => {
  const lastNavigation = useLastLocation();
  React.useEffect(() => {
    if (!isAsync && lastNavigation !== null) {
      routeFocusTimer = accessibleRouteChangeHandler();
    }
    return () => {
      window.clearTimeout(routeFocusTimer);
    };
  }, [isAsync, lastNavigation]);
};

const RouteWithTitleUpdates = ({ component: Component, isAsync = false, title, ...rest }: IAppRoute) => {
  useA11yRouteChange(isAsync);
  useDocumentTitle(title);

  function routeWithTitle(routeProps: RouteComponentProps) {
    return <Component {...rest} {...routeProps} />;
  }

  return <Route render={routeWithTitle} {...rest} />;
};

const PageNotFound = ({ title }: { title: string }) => {
  useDocumentTitle(title);
  return <Route component={NotFound} />;
};

const getFlattenedRoutes = (routes) => {
  const flattenedRoutes: IAppRoute[] = routes.reduce(
    (flattened, route) => [...flattened, ...(route.routes ? route.routes : [route])],
    [] as IAppRoute[]
  );

  return flattenedRoutes;
};

export const AppRoutes = (): React.ReactElement => {
  const [_routes, setRoutes] = React.useState(undefined);

  React.useEffect(() => {
    fetch('routes.json', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then((myJson) => {
        console.log('Menu found!', myJson);
        setRoutes(myJson['routes']);
      })
      .catch((error) => {
        console.log('Menu not found! ', error);
        // setRoutes(undefined);
      });
  }, []);

  console.log(_routes);

  if (!_routes)
    return (
      <LastLocationProvider>
        <Switch>
          <PageSection>
            <Spinner isSVG />
          </PageSection>
        </Switch>
      </LastLocationProvider>
    );

  routes = _routes;

  const flattenedRoutes = getFlattenedRoutes(_routes);

  flattenedRoutes.forEach((route) => {
    if (!route.routes && typeof route.component == 'string') {
      route.component = componentDict[route.component];
    }
  });

  console.log('flattenedRoutes: ', flattenedRoutes);

  return (
    <LastLocationProvider>
      <Switch>
        {flattenedRoutes.map(({ path, exact, component, title, isAsync, src }, idx) => (
          <RouteWithTitleUpdates
            path={path}
            exact={exact}
            component={component}
            key={idx}
            title={title}
            isAsync={isAsync}
            src={src}
          />
        ))}
        {/* <Redirect from="/" to="/dashboard" /> */}
        <PageNotFound title="404 Page Not Found" />
      </Switch>
    </LastLocationProvider>
  );
};
