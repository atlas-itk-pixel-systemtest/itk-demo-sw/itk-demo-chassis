import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app/index';
import './style.css';

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
