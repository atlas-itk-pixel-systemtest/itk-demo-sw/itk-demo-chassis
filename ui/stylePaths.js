const path = require('path')
module.exports = {
  stylePaths: [
    path.resolve(__dirname, 'src'),
    // path.resolve(__dirname, 'node_modules/patternfly'),
    // path.resolve(__dirname, 'node_modules/@patternfly/patternfly'),
    path.resolve(__dirname, 'node_modules/@patternfly/react-styles/css'),
    path.resolve(__dirname, 'node_modules/@patternfly/react-core/dist/styles/base.css')
    // path.resolve(__dirname, 'node_modules/@patternfly/react-core/dist/styles/base-no-reset.css'),
    // path.resolve(__dirname, 'node_modules/@patternfly/react-core/dist/esm/@patternfly/patternfly'),
    // path.resolve(__dirname, 'node_modules/@patternfly/react-core/node_modules/@patternfly/react-styles/css'),
    // path.resolve(__dirname, 'node_modules/@patternfly/react-table/node_modules/@patternfly/react-styles/css'),
    // path.resolve(__dirname, 'node_modules/@patternfly/react-inline-edit-extension/node_modules/@patternfly/react-styles/css'),
    // path.resolve(__dirname, 'node_modules@patternfly/react-styles/css/utilities/Accessibility/accessibility.css'),
    // path.resolve(__dirname, 'node_modules@patternfly/react-styles/css/utilities/Spacing/spacing.css'),
    // path.resolve(__dirname, 'node_modules@patternfly/react-styles/css/components/Dropdown/dropdown.css')
  ]
}
