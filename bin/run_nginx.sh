#!/bin/bash

# require sudo
if [ $EUID != 0 ]; then
  sudo "$0" "$@"
  exit $?
fi

# TO DO: install nginx if not installed
# yum install nginx
# apt-get install nginx

# TO DO: open firewall if closed (in script?)

# rm /etc/nginx/sites-enabled/default
cp ./default.nginx /etc/nginx/sites-available
# ln -s /etc/nginx/sites-available/default.nginx /etc/nginx/sites-enabled/default.nginx
nginx -t
cat /etc/nginx/sites-enabled/default.nginx

# TO DO: support systemd
# systemctl enable nginx
# systemctl start nginx
# systemctl is-active nginx
# systemctl status nginx

nginx -s reload
lsof -i TCP:80
