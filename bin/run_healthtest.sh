#!/bin/bash

max=$(($1 / $2))
counter=0
echo $max x "$2"
while [ $counter -le $max ]; do
  t=0
  while [ $t -le "$2" ]; do
    curl -s -X 'GET' 'http://localhost:5000/api/health' -H 'accept: application/json' >/dev/null &
    # echo $counter
    t=$((t + 1))
  done
  counter=$((counter + 1))
  wait
done
