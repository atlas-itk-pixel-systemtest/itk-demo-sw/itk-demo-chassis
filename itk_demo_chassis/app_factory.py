import logging
import os

from connexion import FlaskApp
from connexion.exceptions import ProblemException
from flask import redirect
from itk_demo_chassis.itk_demo_service import ITkDemoService

# from connexion.resolver import RestyResolver


logging.basicConfig(level=logging.INFO)

itkdemosvc = ITkDemoService()


def create_app():

    cwd = os.getcwd()

    connexion_app = FlaskApp(
        __name__,
        # specification_dir=cwd+'/openapi/',
        options={
            "swagger_ui": True,
            # 'swagger_url': '/',
        },
        arguments={"this_server": "http://randomserver:5000/api"},
        server_args={
            "static_folder": cwd + "/dist",
            "static_url_path": "",  # cwd+'/',
            # 'instance_relative_config': True,
        },
        debug=True,
    )

    connexion_app.add_error_handler(404, flask_error_response)
    connexion_app.add_error_handler(ProblemException, connexion_error_response)

    flask_app = connexion_app.app
    # flask_app.config.from_object('app.default_config')
    # flask_app.config.from_pyfile('app.cfg.py')

    @flask_app.route("/", defaults={"path": ""})
    @flask_app.route("/<path:path>")
    def catch_all(path):
        return flask_app.send_static_file("index.html")

    # itkdemosvc.init_app(flask_app)

    with flask_app.app_context():
        try:
            connexion_app.add_api(
                cwd + "/openapi/openapi.yaml",
                # resolver=RestyResolver('app'),
            )
        except Exception as e:
            print(e)
            flask_app.logger.error("Can't find OpenAPI specification!")
            # still serve the Flask app
            # sys.exit()

        # itkdemosvc.create_menu_from_manifest()
        # itkdemosvc.publish_manifest()
        # itkdemosvc.get_all_manifests()
        # itkdemosvc.merge_manifest_to_menu()

    return flask_app


def flask_error_response(error):
    return redirect("/")
    # dic = {
    #     "data": error.description,
    #     "status": error.code
    # }
    # flask_app.send_static_file("index.html")
    # return dic, error.code, {'content-type': 'application/json'}


def connexion_error_response(error):
    dic = {"data": error.detail, "status": error.status}
    return dic, error.status, {"content-type": "application/json"}


# if __name__ == "__main__":
#     app = create_app()
#     app.run(host="0.0.0.0", port=5000, debug=True)
