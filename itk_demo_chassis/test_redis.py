from redis import Redis

# , ConnectionError

try:
    r = Redis(host="localhost", port=6379, db=0)
except Exception as e:
    print(e)

print(r)

pong = False
try:
    pong = r.ping()
except Exception as e:
    print(e)

print(pong)
