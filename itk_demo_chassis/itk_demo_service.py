import json
import logging
from pprint import pformat
from urllib.parse import urlparse

# , ConnectionError
from flask import Flask, _app_ctx_stack, current_app
from redis import Redis

# from redis import connection


class ITkDemoService:
    def __init__(self, app=None):
        self.app = app
        if app:
            self.init_app(app)
            app.logger.info(f"__init__ {app}")

    def init_app(self, app: Flask):
        app.logger.setLevel(logging.DEBUG)
        app.logger.debug(f"init_app {app}")
        app.config.setdefault("ITKDEMOSVC_REDIS", "http://localhost:6379/")
        app.config.setdefault("ITKDEMOSVC_MANIFEST", "manifest.json")
        app.teardown_appcontext(self.teardown)

    def connect(self):
        current_app.logger.debug("CONNECT")
        redis_url = current_app.config["ITKDEMOSVC_REDIS"]
        pr = urlparse(redis_url)
        r = Redis(host=pr.hostname, port=pr.port, db=0)
        try:
            r.ping()
        # except ConnectionError as e:
        #     current_app.logger.exception(e)
        except Exception as e:
            current_app.logger.warning(f"Can't connect to REDIS: {e}")
            return False
        current_app.logger.info(f"Connected to REDIS: {r}")
        return r

    def teardown(self, exception):
        # ctx = _app_ctx_stack.top
        # self.app.logger.info(f'{__name__} teardown {ctx}')
        return

    @property
    def connection(self):
        current_app.logger.debug("CONNECTION")
        ctx = _app_ctx_stack.top
        if ctx is not None:
            if not hasattr(ctx, "redis"):
                ctx.redis = self.connect()
            return ctx.redis
        return False

    def load_manifest(self):
        """Load manifest from disk."""
        current_app.logger.debug("LOAD MANIFEST")
        self.manifest = {}
        manifest_path = current_app.config["ITKDEMOSVC_MANIFEST"]
        fp = open(manifest_path)
        # print(fp.readlines())
        try:
            self.manifest = json.load(fp)
            current_app.logger.info(f"Manifest loaded from {manifest_path}.")
        except Exception as e:
            current_app.logger.exception(e)
        return self.manifest

    def publish_manifest(self):
        """Publish manifest to REDIS."""
        current_app.logger.debug("PUBLISH MANIFEST")
        json_manifest = json.dumps(self.manifest)
        r = self.connection
        if r:
            result = r.set(f'manifests/{self.manifest["name"]}', json_manifest)
            current_app.logger.info(f"REDIS: {result}")

    def get_manifest(self):
        """Read back manifest from REDIS."""
        current_app.logger.debug("GET MANIFEST")
        r = self.connection
        if r:
            json_manifest = r.get("manifest")
            manifest = json.loads(json_manifest.decode("utf-8"))
            return manifest

    def get_all_manifests(self):
        """Find all manifests in REDIS."""
        current_app.logger.debug("GET ALL MANIFESTS")
        manifests = []
        r = self.connection
        if r:
            mkeys = r.keys("manifest*")
            for mkey in mkeys:
                json_manifest = r.get(mkey)
                manifest = json.loads(json_manifest.decode("utf-8"))
                manifests.append(manifest)
        ms = pformat(manifests, indent=2)
        current_app.logger.debug(f"Manifests: {ms}")
        return manifests

    def merge_manifest_to_menu(self):
        """Extract entries relevant for menu from manifest and merge into menu."""
        print(self.menu)
        menu_list = self.menu["routes"]

        # remove entry if already there

        # for index, namedict in enumerate(menu_list):
        #     if ('name' not in self.manifest.keys()) or (
        #             'name' not in menu_list[index].keys()):
        #         continue
        #     if self.manifest['name'] == menu_list[index]['name']:
        #         del menu_list[index]
        #         break

        mm = {}
        mm["label"] = self.manifest["label"]
        # mm['title'] = f'{__name__} | ' + self.manifest['label']
        # mm['items'] = []
        mm["routes"] = []

        for service in self.manifest["services"]:
            sm = {}
            tags = service.get("tags")
            if tags and "webui" in tags:
                sm["label"] = service["label"]
                # path = service["path"]
                # sm["path"] = path.replace("0.0.0.0", "localhost")
                # mm['items'].append(sm)

        menu_list.append(mm)

        self.menu = {"menu": menu_list}

        return self.menu

    def create_menu_from_manifest(self):
        """Create initial version of menu.json from manifest.json."""
        self.menu = {"routes": []}
        self.manifest = self.load_manifest()
        self.menu = self.merge_manifest_to_menu()
        json.dump(self.menu, open("./dist/routes.json", "w"))

        json_menu = json.dumps(self.menu, indent=2)
        current_app.logger.info(f"Initial menu created: {json_menu}")
