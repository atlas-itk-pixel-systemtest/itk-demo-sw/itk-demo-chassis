# ITk Demonstrator Chassis

Cross-cutting concerns for demonstrator microservices.
## Instructions

1. Install DeMi bootstrapping tool.

```bash
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi.git
source demi/setup.sh
```

1. Install frontend chain

```bash
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-chassis
cd itk-demo-chassis
demi nodejs install
demi nodejs build
demi nodejs run
```

1. Install Python Flask server

```bash
demi python install
poetry run chassis
```

